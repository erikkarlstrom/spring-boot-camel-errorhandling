package errorhandling;

import org.apache.camel.Consumer;
import org.apache.camel.Endpoint;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.remote.RemoteFilePollingConsumerPollStrategy;
import org.apache.camel.spi.PollingConsumerPollStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class FtpRouteExamples extends RouteBuilder {

    final HealthCheckExample ftpHealthCheck;

    public FtpRouteExamples(HealthCheckExample ftpHealthCheck) {
        this.ftpHealthCheck = ftpHealthCheck;
    }


    @Bean(name = "ftpHealthCheckAware")
    public PollingConsumerPollStrategy strategy() {
        PollingConsumerPollStrategy strategy = new MyPollingConsumerPollStrategy(ftpHealthCheck);
        return strategy;
    }

    @Override
    public void configure() throws Exception {
        // case 6 polling consumer strategy no Exception
        from("ftp://ftp.local/?username=one&password=1234_superman&passiveMode=true&pollStrategy=#ftpHealthCheckAware")
                .routeId("ftp-pc-with-polling-strategy")
                .to("log:ftp-pc-with-health");
        // case 7 polling consumer strategy with Exception
        from("ftp://ftp.local/?username=one&password=1234_superman&passiveMode=true&throwExceptionOnConnectFailed=true&pollStrategy=#ftpHealthCheckAware")
                .routeId("ftp-pc-with-exception-and-polling-strategy")
                .to("log:ftp-pc-with-health");
    }

    public class MyPollingConsumerPollStrategy extends RemoteFilePollingConsumerPollStrategy {

        private final HealthCheckExample ftpHealthCheck;

        public MyPollingConsumerPollStrategy(HealthCheckExample ftpHealthCheck){
            this.ftpHealthCheck = ftpHealthCheck;
        }

        @Override
        public void commit(Consumer consumer, Endpoint endpoint, int polledMessages){
            ftpHealthCheck.reset();
            System.out.println("commit");
            super.commit(consumer, endpoint, polledMessages);
        }

        @Override
        public boolean rollback(Consumer consumer, Endpoint endpoint, int retryCounter, Exception e) throws Exception {
            System.out.println(e.getCause() + " we could check which exception");
            ftpHealthCheck.error();
            return super.rollback(consumer, endpoint, retryCounter, e);
        }
    }
}
