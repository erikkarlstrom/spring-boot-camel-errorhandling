package errorhandling;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HealthCheckConfig {
    @Bean(name = "healthCheckExample")
    public HealthCheckExample newHealthCheck(){
        return new HealthCheckExample();
    }
}
