package errorhandling;

import org.apache.camel.health.HealthCheckResultBuilder;
import org.apache.camel.impl.health.AbstractHealthCheck;

import java.util.Map;

public class HealthCheckExample extends AbstractHealthCheck {
    private boolean up = true;

    protected HealthCheckExample() {
        super("custom", "example-health");
    }

    @Override
    protected void doCall(HealthCheckResultBuilder builder, Map<String, Object> options) {
        builder.detail("example", "Chaos monkey was here");
        if (up) {
            builder.up();
        } else {
            builder.down();
        }
    }

    public String error() {
        up = false;
        return "We got som ftp problems";
    }

    public String reset() {
        up = true;
        return "No problems yet.";
    }
}
