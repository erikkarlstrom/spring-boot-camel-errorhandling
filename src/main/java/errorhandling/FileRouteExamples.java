package errorhandling;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class FileRouteExamples extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:deadletter?level=WARN"));

        // 1. Hello world file route
        from("file:data-in?noop=true").to("log:poll-dir-log");
        // 2. Failing Consumer, throws exception, not handled by ErrorHandler
        from("file:doesnt-exist?noop=true&autoCreate=false&directoryMustExist=true")
                .to("log:poll-dir-log");
        // 3. Failing Consumer with &bridgeErrorHandler=true handled by ErrorHandler
        from("file:doesnt-exist?noop=true&autoCreate=false&directoryMustExist=true&bridgeErrorHandler=true")
                .to("log:poll-dir-log");
        // 4. Failing pollEnrich producer triggering ErrorHandler
        from("timer:file?period=10s")
                .pollEnrich("file:doesnt-exist?noop=true&autoCreate=false&directoryMustExist=true")
                .to("log:result");
        // 5. onException Catches exception, skips ErrorHandler, enables routing logic from Exception type
        from("file:data-another-in?noop=true")
                .onException(Exception.class).to("log:exception").end()
                .to("log:poll-dir-log");
    }
}
